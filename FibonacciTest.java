import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FibonacciTest {
    
@Test 
public void testSequencefails(){
    Sequence c =  new FibonacciSequence(2,3);
    assertEquals(0,((FibonacciSequence)c).getTerm(1));
}
@Test
public void testNum1(){
     Sequence fib = new FibonacciSequence(-1, -6);
     assertEquals(-1,((FibonacciSequence)fib).getFirNum());    
}

@Test
public void testNum2(){
     Sequence fib = new FibonacciSequence(-1, -6);
     assertEquals(-6,((FibonacciSequence)fib).getSecNum());   
}

@Test
public void testGetTerm() {
    Sequence fib = new FibonacciSequence(2, 3);
    assertEquals(13,((FibonacciSequence)fib).getTerm(5));
}

}

