//Xuanru Yang, 1942014
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SkiponacciSequenceTest {
    @Test
    public void testFail() {
        SkiponacciSequence skip = new SkiponacciSequence(1, 2, 3);
        assertEquals(0, ((SkiponacciSequence)skip).getTerm(7));
    }

    @Test
    public void testNum1(){
        Sequence skip = new SkiponacciSequence(-1, 4, 2);
        assertEquals(-1,((SkiponacciSequence)skip).getFirstTerm());    
    }

    @Test
    public void testNum2(){
        Sequence skip = new SkiponacciSequence(-1, 4, 2);
        assertEquals(4,((SkiponacciSequence)skip).getSecondTerm());   
    }

    @Test
    public void testNum3(){
        Sequence skip = new SkiponacciSequence(-1, 4, 2);
        assertEquals(2,((SkiponacciSequence)skip).getThirdTerm()); 
    }

    @Test
    public void testGetTerm() {
        Sequence skip = new SkiponacciSequence(-1, 2, 3);
        assertEquals(9,((SkiponacciSequence)skip).getTerm(8));
    }
}
        