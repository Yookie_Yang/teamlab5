import java.util.*;

public class Application {
    public static void main(String[] args){

        Scanner reader = new Scanner(System.in);
        String s = reader.next();
        reader.close();
        String[] st = s.split(";");
        System.out.println(st.length);
        Sequence[] se = parse(s);
        print(se, 10);

    }
    //print method
    public static void print(Sequence[] a, int n) {
      for(Sequence s : a){
          System.out.println(s.getTerm(n));
      }

    }
    //parse method that returns the sequence[]
    public static Sequence[] parse(String s){
      String[] str = s.split(";");
      System.out.println(s.length());
      Sequence[] se = new Sequence[0]; //fixed length 
      int j = 0;
      for(int i = 0; i< str.length;i++){
        //int number = Integer.parseInt(str[i]);
        if(str[i].contains("Fib")){
          Sequence[] temp = new Sequence[se.length+1];
          for(int x = 0; x < se.length; x++){
            temp[x] = se[x];
          }
          se = temp;
          int a = Integer.parseInt(str[i+1]);
          int b = Integer.parseInt(str[i+2]);
          se[j] = new FibonacciSequence(a,b);
          j++;
        }
        else if(str[i].contains("Skip")){
          Sequence[] temp = new Sequence[se.length+1];
          for(int x = 0; x < se.length; x++){
            temp[x] = se[x];
          }
          se = temp;
          int a = Integer.parseInt(str[i+1]);
          int b = Integer.parseInt(str[i+2]);
          int c = Integer.parseInt(str[i+3]);
          se[j] = new SkiponacciSequence(a,b,c);
          j++;
        }
      }
       return se;
    }
}

