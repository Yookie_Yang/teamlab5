//Xuanru Yang, 1942014

public class FibonacciSequence extends Sequence{

    private int firNum;

    private int secNum;

    public FibonacciSequence (int firNum, int secNum) {
        this.firNum = firNum;
        this.secNum = secNum;
    }

    public int getFirNum() {
        return firNum;
    }

    public int getSecNum() {
        return secNum;
    }

    @Override
    public int getTerm(int n) {
        int num1 = this.firNum;
        int num2 = this.secNum;
        int result = num1 + num2;
        if(n < 1) {
            throw new IllegalArgumentException("The number must be positive!");
        }
        else if(n == 1) {
            return num1;
        }
        else if(n == 2) {
            return num2;
        }
        else {
            for(int i = 2; i < n; i++) {
                result = num1 + num2;
                num1 = num2;
                num2 = result;
            }
            return result;
        }
    }
}