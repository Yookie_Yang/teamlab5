public class SkiponacciSequence extends Sequence{

    private int thirdTerm;
    private int firstTerm;
    private int secondTerm;
public SkiponacciSequence(int firstTerm, int secondTerm, int thirdTerm){
    this.secondTerm = secondTerm;
    this.firstTerm = firstTerm;
    this.thirdTerm = thirdTerm;
}

    public int getFirstTerm() {
        return firstTerm;
    }

    public int getSecondTerm() {
        return secondTerm;
    }

    public int getThirdTerm() {
        return thirdTerm;
    }

@Override
public int getTerm(int n){
        int b0= this.firstTerm;
        int b1= this.secondTerm;
        int b2= this.thirdTerm;
      int result = b0 + b1;
    if(n <0 || n==0){
        throw new IllegalArgumentException("The input should be greater than 0!");
    }
    if(n==1){
        return this.firstTerm;
    }
    else if(n==2){
        return this.secondTerm;
    }
    else if(n==3){
        return this.thirdTerm;
    }
    else{
        for(int i = 3; i< n; i++){
            result = b0 + b1;
            b0 = b1;
            b1 = b2;
            b2 = result;
        }
        return result;

    }
}


}
